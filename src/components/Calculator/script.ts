// External dependencies
import Decimal from 'decimal.js'
import toFormat from 'toformat'
import { nanoid } from 'nanoid'
import { computed, defineComponent, ref, Ref } from 'vue'

// Project component dependencies
import Button from '../Button.vue'

const Dec = toFormat(Decimal)

// This declaration is merged with the official one. It adds the toFormat method to the Decimal type, which at this point in the code has already been added to the Decimal prototype by the toFormat function call above.
// Obviously, this is a bit of a hacky way of doing things. But after hours of searching online, I was unable to find a better solution. It's kind of weird to be modifying classes dynamically anyway, so it's not surprising that it's difficult to make TypeScript happy when you do.
declare module 'decimal.js' {
	interface Decimal {
		toFormat(
			dp?: number,
			rm?: number,
			fmt?: {
				decimalSeparator?: string
				groupSeparator?: string
				groupSize?: number
				secondaryGroupSize?: number
				fractionGroupSeparator?: string
				fractionGroupSize?: number
			}
		): string
	}
}

enum BinaryOperator {
	Add,
	Divide,
	Multiply,
	Subtract,
}

const OPERATOR_SYMBOLS: Record<BinaryOperator, string> = {
	[BinaryOperator.Add]: '+',
	[BinaryOperator.Subtract]: '−',
	[BinaryOperator.Multiply]: '×',
	[BinaryOperator.Divide]: '∕',
}

enum CalculatorMode {
	Start,
	AfterDigit,
	AfterOperator,
}

type Digit = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9

function doBinaryOperation(n1: Decimal, n2: Decimal, operator: BinaryOperator) {
	switch (operator) {
		case BinaryOperator.Add:
			return n1.plus(n2)
		case BinaryOperator.Subtract:
			return n1.minus(n2)
		case BinaryOperator.Multiply:
			return n1.times(n2)
		case BinaryOperator.Divide:
			return n1.dividedBy(n2)
	}
}

export default defineComponent({
	components: {
		Button,
	},
	setup() {
		const instanceId = nanoid()
		const currentInputString = ref('0')
		const mode = ref(CalculatorMode.Start)
		// User-facing string shown in calculator display. Adds commas to large numbers.
		const display = computed(() => {
			if (
				mode.value === CalculatorMode.Start ||
				mode.value === CalculatorMode.AfterDigit
			) {
				// Parsing "0." into a number will just give 0, so to make it clear to the user that their input was taken, append the locale's decimal separator to the end of the displayed number.
				if (currentInputString.value.endsWith('.')) {
					return new Dec(currentInputString.value).toFormat() + '.'
				} else {
					return new Dec(currentInputString.value).toFormat()
				}
			} else {
				// If the display is currently showing a binary operator, just show it as-is.
				return currentInputString.value
			}
		})
		// You have to type cast here to prevent TypeScript complaining about lastNumber.value not having the private readonly name field. I don't know why.
		const lastNumber = ref(new Dec('0')) as Ref<Decimal>
		const lastBinaryOperator = ref<BinaryOperator | null>(null)

		function reset() {
			lastNumber.value = new Dec('0')
			lastBinaryOperator.value = null
			mode.value = CalculatorMode.Start
			currentInputString.value = '0'
		}

		function backspace() {
			if (mode.value === CalculatorMode.AfterDigit) {
				if (currentInputString.value.length > 1)
					currentInputString.value = currentInputString.value.slice(0, -1)
				else currentInputString.value = '0'
			}
		}

		// When a digit button is pressed.
		function inputDigit(digit: Digit) {
			if (mode.value === CalculatorMode.AfterDigit)
				currentInputString.value += String(digit)
			else currentInputString.value = String(digit) // If at initial state or after operator.

			mode.value = CalculatorMode.AfterDigit
		}

		// When the decimal point button is pressed.
		function inputDecimalPoint() {
			if (
				mode.value === CalculatorMode.AfterDigit &&
				!currentInputString.value.includes('.')
			) {
				currentInputString.value += '.'
			} else if (
				mode.value === CalculatorMode.Start ||
				mode.value === CalculatorMode.AfterOperator
			) {
				currentInputString.value = '0.'
			}

			mode.value = CalculatorMode.AfterDigit
		}

		function negate() {
			if (
				mode.value === CalculatorMode.Start ||
				mode.value === CalculatorMode.AfterDigit
			) {
				currentInputString.value = String(-currentInputString.value)
			}
		}

		function inputBinaryOperator(operator: BinaryOperator) {
			if (
				mode.value === CalculatorMode.AfterDigit ||
				mode.value === CalculatorMode.Start
			) {
				if (lastBinaryOperator.value !== null) {
					lastNumber.value = doBinaryOperation(
						lastNumber.value,
						new Dec(currentInputString.value),
						lastBinaryOperator.value
					)
				} else {
					lastNumber.value = new Dec(currentInputString.value)
				}
				lastBinaryOperator.value = operator
				currentInputString.value = OPERATOR_SYMBOLS[operator]
				mode.value = CalculatorMode.AfterOperator
			}
		}

		const inputAddOperator = () => inputBinaryOperator(BinaryOperator.Add)

		const inputSubtractOperator = () =>
			inputBinaryOperator(BinaryOperator.Subtract)

		const inputMultiplyOperator = () =>
			inputBinaryOperator(BinaryOperator.Multiply)

		const inputDivideOperator = () =>
			inputBinaryOperator(BinaryOperator.Divide)

		function equals() {
			if (
				mode.value !== CalculatorMode.Start &&
				lastBinaryOperator.value !== null
			) {
				const result = doBinaryOperation(
					lastNumber.value,
					new Dec(currentInputString.value),
					lastBinaryOperator.value
				)
				lastNumber.value = result
				lastBinaryOperator.value = null
				currentInputString.value = String(result)
				mode.value = CalculatorMode.Start
			}
		}

		function handleKeyPress({ key }: KeyboardEvent) {
			switch (key) {
				case 'c':
					reset()
					break
				case 'b':
					backspace()
					break
				case 'n':
					negate()
					break
				case '+':
					inputAddOperator()
					break
				case '-':
					inputSubtractOperator()
					break
				case '*':
					inputMultiplyOperator()
					break
				case '/':
					inputDivideOperator()
					break
				case '0':
					inputDigit(0)
					break
				case '1':
					inputDigit(1)
					break
				case '2':
					inputDigit(2)
					break
				case '3':
					inputDigit(3)
					break
				case '4':
					inputDigit(4)
					break
				case '5':
					inputDigit(5)
					break
				case '6':
					inputDigit(6)
					break
				case '7':
					inputDigit(7)
					break
				case '8':
					inputDigit(8)
					break
				case '9':
					inputDigit(9)
					break
				case '.':
					inputDecimalPoint()
					break
				case '=':
					equals()
					break
			}
		}

		return {
			backspace,
			equals,
			handleKeyPress,
			inputAddOperator,
			inputDecimalPoint,
			// I can't type-cast (or use any other TypeScript syntax) in the template, so I do it here.
			inputDigit: (digit: number) => inputDigit(digit as Digit),
			inputDivideOperator,
			inputMultiplyOperator,
			inputSubtractOperator,
			instanceId,
			negate,
			display,
			reset,
		}
	},
})
