declare module '*.vue' {
	import { defineComponent } from 'vue'
	const Component: ReturnType<typeof defineComponent>
	export default Component
}

// Bare minimum typing to make TypeScript happy.
declare module 'toformat' {
	import Decimal from 'decimal.js'

	export default function toFormat(Ctor: typeof Decimal): typeof Decimal
}
