// External dependencies
import { createApp } from 'vue'

// Internal dependencies
import './assets/base-styles.css'
import App from './App.vue'

createApp(App).mount(document.body)
