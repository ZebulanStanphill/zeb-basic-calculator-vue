/* eslint-env node */
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')

const srcFolder = path.resolve(__dirname, 'src')

module.exports = {
	mode: 'production',
	entry: './src/index.ts',
	output: {
		filename: 'bundle.[hash].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.vue$/u,
				include: [srcFolder],
				use: ['vue-loader'],
			},
			{
				test: /\.jsx?$/u,
				include: [srcFolder],
				use: ['babel-loader'],
			},
			{
				test: /\.tsx?$/u,
				include: [srcFolder],
				use: [
					{
						loader: 'ts-loader',
						options: {
							appendTsSuffixTo: [/\.vue$/u],
						},
					},
				],
			},
			{
				test: /\.css$/u,
				include: [srcFolder],
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(ttf|woff2)$/u,
				include: [srcFolder],
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: 'assets/fonts',
						},
					},
				],
			},
		],
	},
	resolve: {
		/* Without this, importing .tsx or .ts files without specifying the extension won't
		work, and TypeScript won't let you use explicit extensions in imports. */
		extensions: ['.tsx', '.ts', '.jsx', '.js', '.vue'],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			meta: {
				viewport: 'width=device-width, initial-scale=1',
			},
			scriptLoading: 'defer',
			title: 'Zeb’s Basic Calculator (Vue version)',
			xhtml: true,
		}),
		new VueLoaderPlugin(),
	],
}
